#ifndef _UI_H
#define _UI_H

#include "../Ecrire.h"

void
ui_file_open_save_dialog_open(Evas_Object *parent, Eina_Bool save, Evas_Smart_Cb func, void *data);

Evas_Object *
ui_find_dialog_open(Evas_Object *parent, Ecrire_Editor *inst);

Evas_Object *
ui_goto_dialog_open(Evas_Object *parent, Ecrire_Editor *inst);

Evas_Object *
ui_settings_open(Evas_Object *parent, Ecrire_Editor *inst);

void
ui_alert_need_saving(Evas_Object *entry, void (*done)(void *data), void *data);

void
ui_alert_warning_popup(Evas_Object *parent, const char *msg);

#endif
