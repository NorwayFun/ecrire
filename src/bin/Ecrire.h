#ifndef _ECRIRE_H
#define _ECRIRE_H

#include <Evas.h>

extern int _ecrire_log_dom;
#define ECRIRE_DEFAULT_LOG_COLOR EINA_COLOR_CYAN

#define CRI(...)      EINA_LOG_DOM_CRIT(_ecrire_log_dom, __VA_ARGS__)
#define ERR(...)      EINA_LOG_DOM_ERR(_ecrire_log_dom, __VA_ARGS__)
#define WRN(...)      EINA_LOG_DOM_WARN(_ecrire_log_dom, __VA_ARGS__)
#define INF(...)      EINA_LOG_DOM_INFO(_ecrire_log_dom, __VA_ARGS__)
#define DBG(...)      EINA_LOG_DOM_DBG(_ecrire_log_dom, __VA_ARGS__)

typedef struct _Ecrire_Editor
{
     Evas_Object *win;
     Evas_Object *entry;
     Evas_Object *busy;

     Evas_Object *search_win;
     Evas_Object *settings_popup;

     struct
     {
        Elm_Object_Item *copy, *cut, *save, *paste, *undo, *redo;
     } menu;

     struct
     {
        Elm_Object_Item *copy, *cut, *save, *paste, *undo, *redo;
     } toolbar;

     const char *filename;
     int unsaved;

     struct
     {
        const char *name;
        int size;
     } font;

     /* Undo stack */
     Eina_List *undo_stack;
     Eina_List *undo_stack_ptr;
     Eina_List *last_saved_stack_ptr;
     Eina_Bool undo_stack_can_merge;

     void *data;
} Ecrire_Editor;

void ecrire_editor_add(const char *filename, const char *font_name, int font_size);
void ecrire_editor_font_save(Ecrire_Editor *inst, const char *font, int size);
void ecrire_editor_font_set(Ecrire_Editor *inst, const char *font, int font_size);
void ecrire_editor_try_save(Ecrire_Editor *inst, void *callback_func);
void ecrire_editor_save(Ecrire_Editor *inst, const char *file);

#ifdef ENABLE_NLS
# include <libintl.h>
# define _(x) gettext(x)
#else
# define _(x) (x)
#endif

#endif
